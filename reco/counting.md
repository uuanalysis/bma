
Total Events: 10000


## Particle from Reaction
From lambda: 
Number of protons: 8585
Number of pi minus: 7240
  
From anti-lambda: 
Number of anti protons: 7005
Number of pi plus: 7415
  
Number of reconstructible lambdas: 6145
Number of reconstructible anti-lambdas: 5115
Number of reconstructible lambdas and anti-lambdas in the same event: 3315


## All Particles
From lambda: 
Number of protons: 10290
Number of pi minus: 9320
  
From anti-lambda: 
Number of anti protons: 7085
Number of pi plus: 9100
  
Number of reconstructible lambdas: 6760
Number of reconstructible anti-lambdas: 5390
Number of reconstructible lambdas and anti-lambdas in the same event: 3790



## Reaction particle, possibility for double counting
From lambda: 
Number of protons: 8655
Number of pi minus: 7300
  
From anti-lambda: 
Number of anti protons: 7040
Number of pi plus: 7475
  
Number of reconstructible lambdas: 6145
Number of reconstructible anti-lambdas: 5115
Number of reconstructible lambdas and anti-lambdas in the same event: 3315


## All particle,  possibility for double counting

From lambda: 
Number of protons: 10590
Number of pi minus: 9745
  
From anti-lambda: 
Number of anti protons: 7160
Number of pi plus: 9420
  
Number of reconstructible lambdas: 6760
Number of reconstructible anti-lambdas: 5390
Number of reconstructible lambdas and anti-lambdas in the same event: 3790
