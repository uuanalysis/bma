#!/bin/sh

if [ $# -lt 1 ]; then
  echo -e "\nMacro File is Necessary.\n"
  echo -e "USAGE: ./root.sh <macro.C>"
  exit 1
fi

FILE=$1
CONTAINER=$HOME/fair/stable/v12.0.3.sif           # v12.0.3, 18.6.3, nov20p1
CONTAINER=$HOME/current/containers/pzfinder.sif    # dev, 18.6.1, nov20p1 (like Virgo)

# singularity exec $CONTAINER root -l -b -q $FILE && exit
singularity run $CONTAINER -c "root -l $FILE" && exit

