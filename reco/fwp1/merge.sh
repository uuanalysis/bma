#!/bin/bash

if [ $# -lt 1 ]; then
  echo -e "\nMinimum One Arguments Are Required\n"
  echo -e "USAGE: ./merge.sh <mode>"
  echo -e "Example: ./merge.sh efwp1"
  exit 1
fi

# set mode:
mode=$1
echo -e "Using Mode: $mode"


# init pandaroot:
CONT=$HOME/current/containers/pzfinder.sif


# merge ntuples:

singularity exec $CONT root -l -b -q merge.C\(\"ntpPiMinus\",\"$mode-PiMinus.root\",\"$mode\_*\_ana.root\"\)
singularity exec $CONT root -l -b -q merge.C\(\"ntpPiPlus\",\"$mode-PiPlus.root\",\"$mode\_*\_ana.root\"\)
singularity exec $CONT root -l -b -q merge.C\(\"ntpProton\",\"$mode-Proton.root\",\"$mode\_*\_ana.root\"\)
singularity exec $CONT root -l -b -q merge.C\(\"ntpAntiProton\",\"$mode-AntiProton.root\",\"$mode\_*\_ana.root\"\)

singularity exec $CONT root -l -b -q merge.C\(\"ntpLambda\",\"$mode-Lambda.root\",\"$mode\_*\_ana.root\"\)
singularity exec $CONT root -l -b -q merge.C\(\"ntpLambdaBar\",\"$mode-LambdaBar.root\",\"$mode\_*\_ana.root\"\)
singularity exec $CONT root -l -b -q merge.C\(\"ntpBestPbarP\",\"$mode-BestPbarP.root\",\"$mode\_*\_ana.root\"\)
singularity exec $CONT root -l -b -q merge.C\(\"ntpMCTruth\",\"$mode-MCTruth.root\",\"$mode\_*\_ana.root\"\)

# compress tuple a file.
tar -czvf $mode.tar.gz $mode-*

