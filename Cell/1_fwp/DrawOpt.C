#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>

void DrawOpt(TH1D* fHist)
{
    /*
    *
    * Funtion to apply styling on histograms.	
    *
    */

    // Number of Bins (X-axis)
    Int_t xbins = fHist->GetNbinsX();

    // Covert to TString for Labels using std::to_string() method from <strings>.
    // For integers std::to_string works fine, for floats use std::stringstream.
    TString nbins(std::to_string(xbins));

    //std::cout << "Number of Bins (X-axis): " << xbins << std::endl;
    std::cout << "Number of Bins (X-axis): " << nbins << std::endl;

    // Bin Width (X-axis)
    Float_t xbin_width = fHist->GetBinWidth(0);


    // Covert to TString for Labels using std::stringstream class & str() method from <sstream>.
    // The std::stringstream has advantage over std::to_string() in case of floats and doubles.

    std::ostringstream oss;
    oss << std::setprecision(2) << std::noshowpoint << xbin_width;

    //std::string str = oss.str();   // oss.str() returns std::string
    //TString bin_width(str);        // std::string to TString

    TString bin_width(oss.str());

    //std::cout << "Bin Width (X-axis): " << xbin_width << std::endl;
    std::cout << "Bin Width (X-axis): " << bin_width << std::endl;

    // ***
    // ***--- Fix the X-axis
    // ***

    //fHist->GetXaxis()->SetRangeUser(0,1.642);    // mass: 1.075,1.16
    fHist->GetXaxis()->SetLabelFont(43);
    fHist->GetXaxis()->SetLabelSize(40);
    fHist->GetXaxis()->SetLabelOffset(0.01);
    fHist->GetXaxis()->SetTitleFont(43);
    fHist->GetXaxis()->SetTitleSize(50);
    fHist->GetXaxis()->SetTitleOffset(1.);   //1.25:: |p| vs theta
    //fHist->GetXaxis()->CenterTitle();

    // ***
    // ***--- Fix the Y-axis
    // ***

    //fHist->GetYaxis()->SetRangeUser(0,60);
    fHist->GetYaxis()->SetLabelFont(43);
    fHist->GetYaxis()->SetLabelSize(40);
    fHist->GetYaxis()->SetLabelOffset(0.01);
    fHist->GetYaxis()->SetTitleFont(43);
    fHist->GetYaxis()->SetTitleSize(50);
    fHist->GetYaxis()->SetTitleOffset(1.);   //1.25:: |p| vs theta
    //fHist->GetYaxis()->CenterTitle();


    // Fix X-axis Labels
    TString xlabel = "x";
    //xlabel = "m(#Lambda) /GeV/c^{2}";             // mass/GeV/c^{2}
    //xlabel = "m(#bar{#Lambda}) /GeV/c^{2}";       // mass/GeV/c^{2}
    //xlabel = "m(#bar{p}p) /GeV/c^{2}";            // mass/GeV/c^{2}
    //xlabel = "#chi^{2}";                          // chi2 (4c)
    //xlabel = "prob";                              // prob (4c)
    //xlabel = "z_{#Lambda} + z_{#bar{#Lambda}}";
    //xlabel = "z(p#pi^{-})";
    xlabel = "p_{l} [GeV/c]";
    //fHist->GetXaxis()->SetTitle(xlabel);


    // Fix Y-axis Labels
    TString ylabel = "y";
    //ylabel = "dN/dM/"+bin_width+" /GeV/c^{2}";    // dN/dM/bin_width/GeV/c^{2}
    //ylabel = "Entries/GeV/c^{2}";                 // Entries/GeV/c^{2}
    //ylabel = "z(#bar{p}#pi^{+})";
    ylabel = "p_{l} [GeV/c]";
    //fHist->GetYaxis()->SetTitle(ylabel);


    // ***
    // *** --- Fix the Title
    // ***

    TString title = "";
    //title = "m(#Lambda_{0})";
    //title = "#bar{#Lambda_{0}}: Mass";
    //title = "#bar{p}p: Mass";
    //title = "z_{#Lambda} + z_{#bar{#Lambda}}";
    //title = "p #bar{p} #rightarrow p#pi^{-} #bar{p}#pi^{+}";
    //title = "p #bar{p} #rightarrow #Lambda#bar{#Lambda} #rightarrow p#pi^{-} #bar{p}#pi^{+}";
    //title = "Signal Sample (Extended Target)";
    fHist->SetTitle(title);
    fHist->SetTitleSize(0.1,"t"); // gStyle->SetTitleSize(0.3,“t”);


    // ***
    // *** Fix the StatBox ***
    // ***

    gStyle->SetOptFit(1);
    gStyle->SetOptStat(1);  // fHist->SetStats(kTRUE);
    gStyle->SetStatX(0.89);
    gStyle->SetStatY(0.89);
    gStyle->SetStatW(0.20);
    gStyle->SetStatH(0.12);
    

    // ***
    // *** Fixing the Layout
    // ***

    fHist->SetLineColor(kBlue);
    //fHist->SetFillColor(kCyan);
    fHist->SetLineWidth(1);

}//end-DrawOpt
