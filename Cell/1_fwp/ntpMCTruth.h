//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Apr 10 23:24:39 2021 by ROOT version 6.16/00
// from TTree ntpMCTruth/MC truth info
// found on file: fwp1-MCTruth.root
//////////////////////////////////////////////////////////

#ifndef ntpMCTruth_h
#define ntpMCTruth_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpMCTruth {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         MC_d0d0_ky;
   Float_t         MC_d1d0_ky;
   Float_t         weightPy2sin;
   Float_t         weightPyBar2sin;
   Float_t         MC_d0px;
   Float_t         MC_d0py;
   Float_t         MC_d0pz;
   Float_t         MC_d0e;
   Float_t         MC_d0p;
   Float_t         MC_d0tht;
   Float_t         MC_d0phi;
   Float_t         MC_d0pt;
   Float_t         MC_d0m;
   Float_t         MC_d0x;
   Float_t         MC_d0y;
   Float_t         MC_d0z;
   Float_t         MC_d0l;
   Float_t         MC_d0chg;
   Float_t         MC_d0pdg;
   Float_t         MC_d0pxcm;
   Float_t         MC_d0pycm;
   Float_t         MC_d0pzcm;
   Float_t         MC_d0ecm;
   Float_t         MC_d0pcm;
   Float_t         MC_d0thtcm;
   Float_t         MC_d0phicm;
   Float_t         MC_d0d0px;
   Float_t         MC_d0d0py;
   Float_t         MC_d0d0pz;
   Float_t         MC_d0d0e;
   Float_t         MC_d0d0p;
   Float_t         MC_d0d0tht;
   Float_t         MC_d0d0phi;
   Float_t         MC_d0d0pt;
   Float_t         MC_d0d0m;
   Float_t         MC_d0d0x;
   Float_t         MC_d0d0y;
   Float_t         MC_d0d0z;
   Float_t         MC_d0d0l;
   Float_t         MC_d0d0chg;
   Float_t         MC_d0d0pdg;
   Float_t         MC_d0d0pxcm;
   Float_t         MC_d0d0pycm;
   Float_t         MC_d0d0pzcm;
   Float_t         MC_d0d0ecm;
   Float_t         MC_d0d0pcm;
   Float_t         MC_d0d0thtcm;
   Float_t         MC_d0d0phicm;
   Float_t         MC_d0d1px;
   Float_t         MC_d0d1py;
   Float_t         MC_d0d1pz;
   Float_t         MC_d0d1e;
   Float_t         MC_d0d1p;
   Float_t         MC_d0d1tht;
   Float_t         MC_d0d1phi;
   Float_t         MC_d0d1pt;
   Float_t         MC_d0d1m;
   Float_t         MC_d0d1x;
   Float_t         MC_d0d1y;
   Float_t         MC_d0d1z;
   Float_t         MC_d0d1l;
   Float_t         MC_d0d1chg;
   Float_t         MC_d0d1pdg;
   Float_t         MC_d0d1pxcm;
   Float_t         MC_d0d1pycm;
   Float_t         MC_d0d1pzcm;
   Float_t         MC_d0d1ecm;
   Float_t         MC_d0d1pcm;
   Float_t         MC_d0d1thtcm;
   Float_t         MC_d0d1phicm;
   Float_t         MC_d1px;
   Float_t         MC_d1py;
   Float_t         MC_d1pz;
   Float_t         MC_d1e;
   Float_t         MC_d1p;
   Float_t         MC_d1tht;
   Float_t         MC_d1phi;
   Float_t         MC_d1pt;
   Float_t         MC_d1m;
   Float_t         MC_d1x;
   Float_t         MC_d1y;
   Float_t         MC_d1z;
   Float_t         MC_d1l;
   Float_t         MC_d1chg;
   Float_t         MC_d1pdg;
   Float_t         MC_d1pxcm;
   Float_t         MC_d1pycm;
   Float_t         MC_d1pzcm;
   Float_t         MC_d1ecm;
   Float_t         MC_d1pcm;
   Float_t         MC_d1thtcm;
   Float_t         MC_d1phicm;
   Float_t         MC_d1d0px;
   Float_t         MC_d1d0py;
   Float_t         MC_d1d0pz;
   Float_t         MC_d1d0e;
   Float_t         MC_d1d0p;
   Float_t         MC_d1d0tht;
   Float_t         MC_d1d0phi;
   Float_t         MC_d1d0pt;
   Float_t         MC_d1d0m;
   Float_t         MC_d1d0x;
   Float_t         MC_d1d0y;
   Float_t         MC_d1d0z;
   Float_t         MC_d1d0l;
   Float_t         MC_d1d0chg;
   Float_t         MC_d1d0pdg;
   Float_t         MC_d1d0pxcm;
   Float_t         MC_d1d0pycm;
   Float_t         MC_d1d0pzcm;
   Float_t         MC_d1d0ecm;
   Float_t         MC_d1d0pcm;
   Float_t         MC_d1d0thtcm;
   Float_t         MC_d1d0phicm;
   Float_t         MC_d1d1px;
   Float_t         MC_d1d1py;
   Float_t         MC_d1d1pz;
   Float_t         MC_d1d1e;
   Float_t         MC_d1d1p;
   Float_t         MC_d1d1tht;
   Float_t         MC_d1d1phi;
   Float_t         MC_d1d1pt;
   Float_t         MC_d1d1m;
   Float_t         MC_d1d1x;
   Float_t         MC_d1d1y;
   Float_t         MC_d1d1z;
   Float_t         MC_d1d1l;
   Float_t         MC_d1d1chg;
   Float_t         MC_d1d1pdg;
   Float_t         MC_d1d1pxcm;
   Float_t         MC_d1d1pycm;
   Float_t         MC_d1d1pzcm;
   Float_t         MC_d1d1ecm;
   Float_t         MC_d1d1pcm;
   Float_t         MC_d1d1thtcm;
   Float_t         MC_d1d1phicm;

   // List of branches
   TBranch        *b_MC_d0d0_ky;   //!
   TBranch        *b_MC_d1d0_ky;   //!
   TBranch        *b_weightPy2sin;   //!
   TBranch        *b_weightPyBar2sin;   //!
   TBranch        *b_MC_d0px;   //!
   TBranch        *b_MC_d0py;   //!
   TBranch        *b_MC_d0pz;   //!
   TBranch        *b_MC_d0e;   //!
   TBranch        *b_MC_d0p;   //!
   TBranch        *b_MC_d0tht;   //!
   TBranch        *b_MC_d0phi;   //!
   TBranch        *b_MC_d0pt;   //!
   TBranch        *b_MC_d0m;   //!
   TBranch        *b_MC_d0x;   //!
   TBranch        *b_MC_d0y;   //!
   TBranch        *b_MC_d0z;   //!
   TBranch        *b_MC_d0l;   //!
   TBranch        *b_MC_d0chg;   //!
   TBranch        *b_MC_d0pdg;   //!
   TBranch        *b_MC_d0pxcm;   //!
   TBranch        *b_MC_d0pycm;   //!
   TBranch        *b_MC_d0pzcm;   //!
   TBranch        *b_MC_d0ecm;   //!
   TBranch        *b_MC_d0pcm;   //!
   TBranch        *b_MC_d0thtcm;   //!
   TBranch        *b_MC_d0phicm;   //!
   TBranch        *b_MC_d0d0px;   //!
   TBranch        *b_MC_d0d0py;   //!
   TBranch        *b_MC_d0d0pz;   //!
   TBranch        *b_MC_d0d0e;   //!
   TBranch        *b_MC_d0d0p;   //!
   TBranch        *b_MC_d0d0tht;   //!
   TBranch        *b_MC_d0d0phi;   //!
   TBranch        *b_MC_d0d0pt;   //!
   TBranch        *b_MC_d0d0m;   //!
   TBranch        *b_MC_d0d0x;   //!
   TBranch        *b_MC_d0d0y;   //!
   TBranch        *b_MC_d0d0z;   //!
   TBranch        *b_MC_d0d0l;   //!
   TBranch        *b_MC_d0d0chg;   //!
   TBranch        *b_MC_d0d0pdg;   //!
   TBranch        *b_MC_d0d0pxcm;   //!
   TBranch        *b_MC_d0d0pycm;   //!
   TBranch        *b_MC_d0d0pzcm;   //!
   TBranch        *b_MC_d0d0ecm;   //!
   TBranch        *b_MC_d0d0pcm;   //!
   TBranch        *b_MC_d0d0thtcm;   //!
   TBranch        *b_MC_d0d0phicm;   //!
   TBranch        *b_MC_d0d1px;   //!
   TBranch        *b_MC_d0d1py;   //!
   TBranch        *b_MC_d0d1pz;   //!
   TBranch        *b_MC_d0d1e;   //!
   TBranch        *b_MC_d0d1p;   //!
   TBranch        *b_MC_d0d1tht;   //!
   TBranch        *b_MC_d0d1phi;   //!
   TBranch        *b_MC_d0d1pt;   //!
   TBranch        *b_MC_d0d1m;   //!
   TBranch        *b_MC_d0d1x;   //!
   TBranch        *b_MC_d0d1y;   //!
   TBranch        *b_MC_d0d1z;   //!
   TBranch        *b_MC_d0d1l;   //!
   TBranch        *b_MC_d0d1chg;   //!
   TBranch        *b_MC_d0d1pdg;   //!
   TBranch        *b_MC_d0d1pxcm;   //!
   TBranch        *b_MC_d0d1pycm;   //!
   TBranch        *b_MC_d0d1pzcm;   //!
   TBranch        *b_MC_d0d1ecm;   //!
   TBranch        *b_MC_d0d1pcm;   //!
   TBranch        *b_MC_d0d1thtcm;   //!
   TBranch        *b_MC_d0d1phicm;   //!
   TBranch        *b_MC_d1px;   //!
   TBranch        *b_MC_d1py;   //!
   TBranch        *b_MC_d1pz;   //!
   TBranch        *b_MC_d1e;   //!
   TBranch        *b_MC_d1p;   //!
   TBranch        *b_MC_d1tht;   //!
   TBranch        *b_MC_d1phi;   //!
   TBranch        *b_MC_d1pt;   //!
   TBranch        *b_MC_d1m;   //!
   TBranch        *b_MC_d1x;   //!
   TBranch        *b_MC_d1y;   //!
   TBranch        *b_MC_d1z;   //!
   TBranch        *b_MC_d1l;   //!
   TBranch        *b_MC_d1chg;   //!
   TBranch        *b_MC_d1pdg;   //!
   TBranch        *b_MC_d1pxcm;   //!
   TBranch        *b_MC_d1pycm;   //!
   TBranch        *b_MC_d1pzcm;   //!
   TBranch        *b_MC_d1ecm;   //!
   TBranch        *b_MC_d1pcm;   //!
   TBranch        *b_MC_d1thtcm;   //!
   TBranch        *b_MC_d1phicm;   //!
   TBranch        *b_MC_d1d0px;   //!
   TBranch        *b_MC_d1d0py;   //!
   TBranch        *b_MC_d1d0pz;   //!
   TBranch        *b_MC_d1d0e;   //!
   TBranch        *b_MC_d1d0p;   //!
   TBranch        *b_MC_d1d0tht;   //!
   TBranch        *b_MC_d1d0phi;   //!
   TBranch        *b_MC_d1d0pt;   //!
   TBranch        *b_MC_d1d0m;   //!
   TBranch        *b_MC_d1d0x;   //!
   TBranch        *b_MC_d1d0y;   //!
   TBranch        *b_MC_d1d0z;   //!
   TBranch        *b_MC_d1d0l;   //!
   TBranch        *b_MC_d1d0chg;   //!
   TBranch        *b_MC_d1d0pdg;   //!
   TBranch        *b_MC_d1d0pxcm;   //!
   TBranch        *b_MC_d1d0pycm;   //!
   TBranch        *b_MC_d1d0pzcm;   //!
   TBranch        *b_MC_d1d0ecm;   //!
   TBranch        *b_MC_d1d0pcm;   //!
   TBranch        *b_MC_d1d0thtcm;   //!
   TBranch        *b_MC_d1d0phicm;   //!
   TBranch        *b_MC_d1d1px;   //!
   TBranch        *b_MC_d1d1py;   //!
   TBranch        *b_MC_d1d1pz;   //!
   TBranch        *b_MC_d1d1e;   //!
   TBranch        *b_MC_d1d1p;   //!
   TBranch        *b_MC_d1d1tht;   //!
   TBranch        *b_MC_d1d1phi;   //!
   TBranch        *b_MC_d1d1pt;   //!
   TBranch        *b_MC_d1d1m;   //!
   TBranch        *b_MC_d1d1x;   //!
   TBranch        *b_MC_d1d1y;   //!
   TBranch        *b_MC_d1d1z;   //!
   TBranch        *b_MC_d1d1l;   //!
   TBranch        *b_MC_d1d1chg;   //!
   TBranch        *b_MC_d1d1pdg;   //!
   TBranch        *b_MC_d1d1pxcm;   //!
   TBranch        *b_MC_d1d1pycm;   //!
   TBranch        *b_MC_d1d1pzcm;   //!
   TBranch        *b_MC_d1d1ecm;   //!
   TBranch        *b_MC_d1d1pcm;   //!
   TBranch        *b_MC_d1d1thtcm;   //!
   TBranch        *b_MC_d1d1phicm;   //!

   ntpMCTruth(TTree *tree=0);
   virtual ~ntpMCTruth();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   Double_t fMass0_lam = TDatabasePDG::Instance()->GetParticle(-3122)->Mass();
   
   
   //***
   //----- Histograms: Kinematics
   //***
   
   // ----- Final States
   TH2F *hpbar_MomTht;
   TH2F *hpbar_PzPt;
   TH2F *hpbar_xy;
   TH1F *hpbar_mass;
      
   TH2F *hpiplus_MomTht;
   TH2F *hpiplus_PzPt;
   TH2F *hpiplus_xy;
   TH1F *hpiplus_mass;
   
   TH2F *hp_MomTht;
   TH2F *hp_PzPt;
   TH2F *hp_xy;
   TH1F *hp_mass;
   
   TH2F *hpiminus_MomTht;
   TH2F *hpiminus_PzPt;
   TH2F *hpiminus_xy;
   TH1F *hpiminus_mass;
   
   TH2F *hpbar_zR;         // l = R = Perp()
   TH2F *hp_zR;            // l = R = Perp()
   TH2F *hpiplus_zR;       // l = R = Perp()
   TH2F *hpiminus_zR;      // l = R = Perp()

   
   // All particles together
   TH2F *MomThtAll;
   TH2F *PzPtAll;
   TH2F *zRAll;
   
   // ----- LLBar
   TH2F *hlam0_p4;
   TH2F *hlam0bar_p4;
   TH1F *hlam0_thtcm;
   TH1F *hlam0bar_thtcm;
   
   // ***
   // ----- Histograms: StartVertex
   // ***
   TH1D *hMC_d0z;
   TH1D *hMC_d1z;
   
   // ***
   // ----- Histograms: 2D Scatter Plots
   // ***
   TH2D *hMC_d0z_d1z;
   TH2D *hMC_d0d0z_d0d1z;
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
   
};

#endif

#ifdef ntpMCTruth_cxx
ntpMCTruth::ntpMCTruth(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.

   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TString path = "";
   TFile* hfile = TFile::Open(path+"ntpMCTruth.root", "RECREATE");
   
   //*** 
   //----- Histograms: Kinematics
   //***

   hpbar_MomTht = new TH2F("hpbar_MomTht","|p| vs #theta dist. (anti-proton);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
   hpbar_PzPt = new TH2F("hpbar_PzPt","p_{z} vs p_{t} dist. (anti-proton);p_{z} [GeV/c];p_{t} [GeV/c]",500,0,1.65,500,0,1.65);
   hpbar_xy = new TH2F("hpbar_xy","x vs y dist. (anti-proton);x [cm];y [cm]",500,-100,100,500,-100,100);
   hpbar_mass = new TH1F("hpbar_mass","mass dist. (anti-proton);m(#bar{p}) /GeV/c^{2};Entries /GeV/c^{2}",500,0,2);
   
   hpiplus_MomTht = new TH2F("hpiplus_MomTht","|p| vs #theta dist. (piplus);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
   hpiplus_PzPt = new TH2F("hpiplus_PzPt","p_{z} vs p_{t} dist. (piplus);p_{z} [GeV/c];p_{t} [GeV/c]",500,0,1.65,500,0,1.65);
   hpiplus_xy = new TH2F("hpiplus_xy","x vs y dist. (piplus);x [cm];y [cm]",500,-100,100,500,-100,100);
   hpiplus_mass = new TH1F("hpiplus_mass","mass dist. (piplus);m(p) /GeV/c^{2};Entries /GeV/c^{2}",500,0,0.5);

   hp_MomTht = new TH2F("hp_MomTht","|p| vs #theta dist. (proton);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
   hp_PzPt = new TH2F("hp_PzPt","p_{z} vs p_{t} dist. (proton);p_{z} [GeV/c];p_{t} [GeV/c]",500,0,1.65,500,0,1.65);
   hp_xy = new TH2F("hp_xy","x vs y dist. (proton);x [cm];y [cm]",500,-100,100,500,-100,100);
   hp_mass = new TH1F("hp_mass","mass dist. (proton);m(p) /GeV/c^{2};Entries /GeV/c^{2}",500,0,2);
    
   hpiminus_MomTht = new TH2F("hpiminus_MomTht","|p| vs #theta dist. (piminus);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
   hpiminus_PzPt = new TH2F("hpiminus_PzPt","p_{z} vs p_{t} dist. (piminus);p_{z} [GeV/c];p_{t} [GeV/c]",500,-1.65,1.65,500,0,1.65);
   hpiminus_xy = new TH2F("hpiminus_xy","x vs y dist. (piminus);x [cm];y [cm]",500,-100,100,500,-100,100);
   hpiminus_mass = new TH1F("hpiminus_mass","mass dist. (piminus);m(p) /GeV/c^{2};Entries /GeV/c^{2}",500,0,0.5);
    
   hpbar_zR = new TH2F("hpbar_zR","z vs r dist. (antiproton);z [cm];R [cm]",500,0,300,500,0,50);
   hpiplus_zR = new TH2F("hpiplus_zR","z vs r dist. (proton);z [cm];R [cm]",500,0,300,500,0,50);
   hp_zR = new TH2F("hp_zR","z vs r dist. (piplus);z [cm];R [cm]",500,0,300,500,0,50);
   hpiminus_zR = new TH2F("hpiminus_zR","z vs r dist. (piminus);z [cm];R [cm]",500,0,300,500,0,50);
   
   MomThtAll = new TH2F("MomThtAll","|p| vs #theta dist. (all);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
   PzPtAll = new TH2F("PzPtAll","p_{z} vs p_{t} dist. (all);p_{z} [GeV/c];p_{t} [GeV/c]",500,0,1.65,500,0,1.65);
   zRAll = new TH2F("zRAll","z vs r dist. (all);z [cm];R [cm]",500,0,300,500,0,50);
   
   // ----- LLBar
   hlam0_p4 = new TH2F("hlam0_p4","Four Momentum Distribution (#Lambda) @1.642 GeV;p_{z} /GeV/c;p_{t} /GeV/c",1000,-0.1,1.5,1000,0,0.5);
   hlam0bar_p4 = new TH2F("hlam0bar_p4","Four Momentum Distribution (#bar{#Lambda}) @1.642 GeV;p_{z} /GeV/c;p_{t} /GeV/c",1000,-0.1,1.5,1000,0,0.5);
   hlam0_thtcm = new TH1F("hlam0_thtcm","Cos #theta_{#Lambda};Cos #theta_{#Lambda};Entries",200,-1.,1.);
   hlam0bar_thtcm = new TH1F("hlam0bar_thtcm","Cos #theta_{#bar{#Lambda}};Cos #theta_{#bar{#Lambda}};Entries",200,-1.,1.);
   
   
   //*** 
   //----- Histograms: StartVertex
   //*** 
       
   //StartVertex.Z() for Lambda, LambdaBar
   hMC_d1z = new TH1D("hMC_d1z","z_{#Lambda} of PV;z_{#Lambda} /cm;Entries /cm",1000,-600,1100); //d1:lam0
   hMC_d0z = new TH1D("hMC_d0z","z_{#bar{#Lambda}} of PV;z_{#bar{#Lambda}} /cm;Entries /cm",1000,-600,1100); //d0: lam0bar

   
   //2D Scatter Plots of StartVertex.Z()
   hMC_d0z_d1z = new TH2D("hMC_d0z_d1z","z-profile of PV;z(#Lambda) /cm;z(#bar{#Lambda}) /cm",1000,-600,1100,1000,-600,1100);  //x: lam0, y: lam0bar
   hMC_d0d0z_d0d1z = new TH2D("hMC_d0d0z_d0d1z","z-profile of PV;z(pi^{-}) /cm;z(#bar{p}) /cm",1000,-600,1100,1000,-600,1100);  //x: lam0, y: lam0bar
   
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------    

   
   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("fwp1-MCTruth.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("fwp1-MCTruth.root");
      }
      f->GetObject("ntpMCTruth",tree);

   }
   
   // Call Init()
   Init(tree);

   // Call Loop() 
   Loop();
   
   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   
   //Hist to ROOT File
   hfile->Write();
   hfile->Close();
   
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
   
}

ntpMCTruth::~ntpMCTruth()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpMCTruth::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpMCTruth::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpMCTruth::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("MC_d0d0_ky", &MC_d0d0_ky, &b_MC_d0d0_ky);
   fChain->SetBranchAddress("MC_d1d0_ky", &MC_d1d0_ky, &b_MC_d1d0_ky);
   fChain->SetBranchAddress("weightPy2sin", &weightPy2sin, &b_weightPy2sin);
   fChain->SetBranchAddress("weightPyBar2sin", &weightPyBar2sin, &b_weightPyBar2sin);
   fChain->SetBranchAddress("MC_d0px", &MC_d0px, &b_MC_d0px);
   fChain->SetBranchAddress("MC_d0py", &MC_d0py, &b_MC_d0py);
   fChain->SetBranchAddress("MC_d0pz", &MC_d0pz, &b_MC_d0pz);
   fChain->SetBranchAddress("MC_d0e", &MC_d0e, &b_MC_d0e);
   fChain->SetBranchAddress("MC_d0p", &MC_d0p, &b_MC_d0p);
   fChain->SetBranchAddress("MC_d0tht", &MC_d0tht, &b_MC_d0tht);
   fChain->SetBranchAddress("MC_d0phi", &MC_d0phi, &b_MC_d0phi);
   fChain->SetBranchAddress("MC_d0pt", &MC_d0pt, &b_MC_d0pt);
   fChain->SetBranchAddress("MC_d0m", &MC_d0m, &b_MC_d0m);
   fChain->SetBranchAddress("MC_d0x", &MC_d0x, &b_MC_d0x);
   fChain->SetBranchAddress("MC_d0y", &MC_d0y, &b_MC_d0y);
   fChain->SetBranchAddress("MC_d0z", &MC_d0z, &b_MC_d0z);
   fChain->SetBranchAddress("MC_d0l", &MC_d0l, &b_MC_d0l);
   fChain->SetBranchAddress("MC_d0chg", &MC_d0chg, &b_MC_d0chg);
   fChain->SetBranchAddress("MC_d0pdg", &MC_d0pdg, &b_MC_d0pdg);
   fChain->SetBranchAddress("MC_d0pxcm", &MC_d0pxcm, &b_MC_d0pxcm);
   fChain->SetBranchAddress("MC_d0pycm", &MC_d0pycm, &b_MC_d0pycm);
   fChain->SetBranchAddress("MC_d0pzcm", &MC_d0pzcm, &b_MC_d0pzcm);
   fChain->SetBranchAddress("MC_d0ecm", &MC_d0ecm, &b_MC_d0ecm);
   fChain->SetBranchAddress("MC_d0pcm", &MC_d0pcm, &b_MC_d0pcm);
   fChain->SetBranchAddress("MC_d0thtcm", &MC_d0thtcm, &b_MC_d0thtcm);
   fChain->SetBranchAddress("MC_d0phicm", &MC_d0phicm, &b_MC_d0phicm);
   fChain->SetBranchAddress("MC_d0d0px", &MC_d0d0px, &b_MC_d0d0px);
   fChain->SetBranchAddress("MC_d0d0py", &MC_d0d0py, &b_MC_d0d0py);
   fChain->SetBranchAddress("MC_d0d0pz", &MC_d0d0pz, &b_MC_d0d0pz);
   fChain->SetBranchAddress("MC_d0d0e", &MC_d0d0e, &b_MC_d0d0e);
   fChain->SetBranchAddress("MC_d0d0p", &MC_d0d0p, &b_MC_d0d0p);
   fChain->SetBranchAddress("MC_d0d0tht", &MC_d0d0tht, &b_MC_d0d0tht);
   fChain->SetBranchAddress("MC_d0d0phi", &MC_d0d0phi, &b_MC_d0d0phi);
   fChain->SetBranchAddress("MC_d0d0pt", &MC_d0d0pt, &b_MC_d0d0pt);
   fChain->SetBranchAddress("MC_d0d0m", &MC_d0d0m, &b_MC_d0d0m);
   fChain->SetBranchAddress("MC_d0d0x", &MC_d0d0x, &b_MC_d0d0x);
   fChain->SetBranchAddress("MC_d0d0y", &MC_d0d0y, &b_MC_d0d0y);
   fChain->SetBranchAddress("MC_d0d0z", &MC_d0d0z, &b_MC_d0d0z);
   fChain->SetBranchAddress("MC_d0d0l", &MC_d0d0l, &b_MC_d0d0l);
   fChain->SetBranchAddress("MC_d0d0chg", &MC_d0d0chg, &b_MC_d0d0chg);
   fChain->SetBranchAddress("MC_d0d0pdg", &MC_d0d0pdg, &b_MC_d0d0pdg);
   fChain->SetBranchAddress("MC_d0d0pxcm", &MC_d0d0pxcm, &b_MC_d0d0pxcm);
   fChain->SetBranchAddress("MC_d0d0pycm", &MC_d0d0pycm, &b_MC_d0d0pycm);
   fChain->SetBranchAddress("MC_d0d0pzcm", &MC_d0d0pzcm, &b_MC_d0d0pzcm);
   fChain->SetBranchAddress("MC_d0d0ecm", &MC_d0d0ecm, &b_MC_d0d0ecm);
   fChain->SetBranchAddress("MC_d0d0pcm", &MC_d0d0pcm, &b_MC_d0d0pcm);
   fChain->SetBranchAddress("MC_d0d0thtcm", &MC_d0d0thtcm, &b_MC_d0d0thtcm);
   fChain->SetBranchAddress("MC_d0d0phicm", &MC_d0d0phicm, &b_MC_d0d0phicm);
   fChain->SetBranchAddress("MC_d0d1px", &MC_d0d1px, &b_MC_d0d1px);
   fChain->SetBranchAddress("MC_d0d1py", &MC_d0d1py, &b_MC_d0d1py);
   fChain->SetBranchAddress("MC_d0d1pz", &MC_d0d1pz, &b_MC_d0d1pz);
   fChain->SetBranchAddress("MC_d0d1e", &MC_d0d1e, &b_MC_d0d1e);
   fChain->SetBranchAddress("MC_d0d1p", &MC_d0d1p, &b_MC_d0d1p);
   fChain->SetBranchAddress("MC_d0d1tht", &MC_d0d1tht, &b_MC_d0d1tht);
   fChain->SetBranchAddress("MC_d0d1phi", &MC_d0d1phi, &b_MC_d0d1phi);
   fChain->SetBranchAddress("MC_d0d1pt", &MC_d0d1pt, &b_MC_d0d1pt);
   fChain->SetBranchAddress("MC_d0d1m", &MC_d0d1m, &b_MC_d0d1m);
   fChain->SetBranchAddress("MC_d0d1x", &MC_d0d1x, &b_MC_d0d1x);
   fChain->SetBranchAddress("MC_d0d1y", &MC_d0d1y, &b_MC_d0d1y);
   fChain->SetBranchAddress("MC_d0d1z", &MC_d0d1z, &b_MC_d0d1z);
   fChain->SetBranchAddress("MC_d0d1l", &MC_d0d1l, &b_MC_d0d1l);
   fChain->SetBranchAddress("MC_d0d1chg", &MC_d0d1chg, &b_MC_d0d1chg);
   fChain->SetBranchAddress("MC_d0d1pdg", &MC_d0d1pdg, &b_MC_d0d1pdg);
   fChain->SetBranchAddress("MC_d0d1pxcm", &MC_d0d1pxcm, &b_MC_d0d1pxcm);
   fChain->SetBranchAddress("MC_d0d1pycm", &MC_d0d1pycm, &b_MC_d0d1pycm);
   fChain->SetBranchAddress("MC_d0d1pzcm", &MC_d0d1pzcm, &b_MC_d0d1pzcm);
   fChain->SetBranchAddress("MC_d0d1ecm", &MC_d0d1ecm, &b_MC_d0d1ecm);
   fChain->SetBranchAddress("MC_d0d1pcm", &MC_d0d1pcm, &b_MC_d0d1pcm);
   fChain->SetBranchAddress("MC_d0d1thtcm", &MC_d0d1thtcm, &b_MC_d0d1thtcm);
   fChain->SetBranchAddress("MC_d0d1phicm", &MC_d0d1phicm, &b_MC_d0d1phicm);
   fChain->SetBranchAddress("MC_d1px", &MC_d1px, &b_MC_d1px);
   fChain->SetBranchAddress("MC_d1py", &MC_d1py, &b_MC_d1py);
   fChain->SetBranchAddress("MC_d1pz", &MC_d1pz, &b_MC_d1pz);
   fChain->SetBranchAddress("MC_d1e", &MC_d1e, &b_MC_d1e);
   fChain->SetBranchAddress("MC_d1p", &MC_d1p, &b_MC_d1p);
   fChain->SetBranchAddress("MC_d1tht", &MC_d1tht, &b_MC_d1tht);
   fChain->SetBranchAddress("MC_d1phi", &MC_d1phi, &b_MC_d1phi);
   fChain->SetBranchAddress("MC_d1pt", &MC_d1pt, &b_MC_d1pt);
   fChain->SetBranchAddress("MC_d1m", &MC_d1m, &b_MC_d1m);
   fChain->SetBranchAddress("MC_d1x", &MC_d1x, &b_MC_d1x);
   fChain->SetBranchAddress("MC_d1y", &MC_d1y, &b_MC_d1y);
   fChain->SetBranchAddress("MC_d1z", &MC_d1z, &b_MC_d1z);
   fChain->SetBranchAddress("MC_d1l", &MC_d1l, &b_MC_d1l);
   fChain->SetBranchAddress("MC_d1chg", &MC_d1chg, &b_MC_d1chg);
   fChain->SetBranchAddress("MC_d1pdg", &MC_d1pdg, &b_MC_d1pdg);
   fChain->SetBranchAddress("MC_d1pxcm", &MC_d1pxcm, &b_MC_d1pxcm);
   fChain->SetBranchAddress("MC_d1pycm", &MC_d1pycm, &b_MC_d1pycm);
   fChain->SetBranchAddress("MC_d1pzcm", &MC_d1pzcm, &b_MC_d1pzcm);
   fChain->SetBranchAddress("MC_d1ecm", &MC_d1ecm, &b_MC_d1ecm);
   fChain->SetBranchAddress("MC_d1pcm", &MC_d1pcm, &b_MC_d1pcm);
   fChain->SetBranchAddress("MC_d1thtcm", &MC_d1thtcm, &b_MC_d1thtcm);
   fChain->SetBranchAddress("MC_d1phicm", &MC_d1phicm, &b_MC_d1phicm);
   fChain->SetBranchAddress("MC_d1d0px", &MC_d1d0px, &b_MC_d1d0px);
   fChain->SetBranchAddress("MC_d1d0py", &MC_d1d0py, &b_MC_d1d0py);
   fChain->SetBranchAddress("MC_d1d0pz", &MC_d1d0pz, &b_MC_d1d0pz);
   fChain->SetBranchAddress("MC_d1d0e", &MC_d1d0e, &b_MC_d1d0e);
   fChain->SetBranchAddress("MC_d1d0p", &MC_d1d0p, &b_MC_d1d0p);
   fChain->SetBranchAddress("MC_d1d0tht", &MC_d1d0tht, &b_MC_d1d0tht);
   fChain->SetBranchAddress("MC_d1d0phi", &MC_d1d0phi, &b_MC_d1d0phi);
   fChain->SetBranchAddress("MC_d1d0pt", &MC_d1d0pt, &b_MC_d1d0pt);
   fChain->SetBranchAddress("MC_d1d0m", &MC_d1d0m, &b_MC_d1d0m);
   fChain->SetBranchAddress("MC_d1d0x", &MC_d1d0x, &b_MC_d1d0x);
   fChain->SetBranchAddress("MC_d1d0y", &MC_d1d0y, &b_MC_d1d0y);
   fChain->SetBranchAddress("MC_d1d0z", &MC_d1d0z, &b_MC_d1d0z);
   fChain->SetBranchAddress("MC_d1d0l", &MC_d1d0l, &b_MC_d1d0l);
   fChain->SetBranchAddress("MC_d1d0chg", &MC_d1d0chg, &b_MC_d1d0chg);
   fChain->SetBranchAddress("MC_d1d0pdg", &MC_d1d0pdg, &b_MC_d1d0pdg);
   fChain->SetBranchAddress("MC_d1d0pxcm", &MC_d1d0pxcm, &b_MC_d1d0pxcm);
   fChain->SetBranchAddress("MC_d1d0pycm", &MC_d1d0pycm, &b_MC_d1d0pycm);
   fChain->SetBranchAddress("MC_d1d0pzcm", &MC_d1d0pzcm, &b_MC_d1d0pzcm);
   fChain->SetBranchAddress("MC_d1d0ecm", &MC_d1d0ecm, &b_MC_d1d0ecm);
   fChain->SetBranchAddress("MC_d1d0pcm", &MC_d1d0pcm, &b_MC_d1d0pcm);
   fChain->SetBranchAddress("MC_d1d0thtcm", &MC_d1d0thtcm, &b_MC_d1d0thtcm);
   fChain->SetBranchAddress("MC_d1d0phicm", &MC_d1d0phicm, &b_MC_d1d0phicm);
   fChain->SetBranchAddress("MC_d1d1px", &MC_d1d1px, &b_MC_d1d1px);
   fChain->SetBranchAddress("MC_d1d1py", &MC_d1d1py, &b_MC_d1d1py);
   fChain->SetBranchAddress("MC_d1d1pz", &MC_d1d1pz, &b_MC_d1d1pz);
   fChain->SetBranchAddress("MC_d1d1e", &MC_d1d1e, &b_MC_d1d1e);
   fChain->SetBranchAddress("MC_d1d1p", &MC_d1d1p, &b_MC_d1d1p);
   fChain->SetBranchAddress("MC_d1d1tht", &MC_d1d1tht, &b_MC_d1d1tht);
   fChain->SetBranchAddress("MC_d1d1phi", &MC_d1d1phi, &b_MC_d1d1phi);
   fChain->SetBranchAddress("MC_d1d1pt", &MC_d1d1pt, &b_MC_d1d1pt);
   fChain->SetBranchAddress("MC_d1d1m", &MC_d1d1m, &b_MC_d1d1m);
   fChain->SetBranchAddress("MC_d1d1x", &MC_d1d1x, &b_MC_d1d1x);
   fChain->SetBranchAddress("MC_d1d1y", &MC_d1d1y, &b_MC_d1d1y);
   fChain->SetBranchAddress("MC_d1d1z", &MC_d1d1z, &b_MC_d1d1z);
   fChain->SetBranchAddress("MC_d1d1l", &MC_d1d1l, &b_MC_d1d1l);
   fChain->SetBranchAddress("MC_d1d1chg", &MC_d1d1chg, &b_MC_d1d1chg);
   fChain->SetBranchAddress("MC_d1d1pdg", &MC_d1d1pdg, &b_MC_d1d1pdg);
   fChain->SetBranchAddress("MC_d1d1pxcm", &MC_d1d1pxcm, &b_MC_d1d1pxcm);
   fChain->SetBranchAddress("MC_d1d1pycm", &MC_d1d1pycm, &b_MC_d1d1pycm);
   fChain->SetBranchAddress("MC_d1d1pzcm", &MC_d1d1pzcm, &b_MC_d1d1pzcm);
   fChain->SetBranchAddress("MC_d1d1ecm", &MC_d1d1ecm, &b_MC_d1d1ecm);
   fChain->SetBranchAddress("MC_d1d1pcm", &MC_d1d1pcm, &b_MC_d1d1pcm);
   fChain->SetBranchAddress("MC_d1d1thtcm", &MC_d1d1thtcm, &b_MC_d1d1thtcm);
   fChain->SetBranchAddress("MC_d1d1phicm", &MC_d1d1phicm, &b_MC_d1d1phicm);
   Notify();
}

Bool_t ntpMCTruth::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpMCTruth::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpMCTruth::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpMCTruth_cxx
