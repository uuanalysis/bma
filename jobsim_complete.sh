#!/bin/bash

# *** Cluster USAGE ***
# sbatch [options] -- jobsim_complete.sh <prefix> <events> <dec> <pbeam> [opt] [mode]

# *** Local USAGE ***
# ./jobsim_complete.sh <prefix> <events> <dec>
# ./jobsim_complete.sh llbar 100 llbar_fwp.DEC


if [ $# -lt 3 ]; then
  echo -e "\nMinimum Three Arguments Are Required\n"
  echo -e "USAGE: sbatch -a<min>-<max> -- jobsim_complete.sh <prefix> <nEvents> <dec>\n"
  echo -e " <min>     : Minimum job number"
  echo -e " <max>     : Maximum job number"
  echo -e " <prefix>  : Prefix of output files"
  echo -e " <nevts>   : Number of events to be simulated"
  echo -e " <dec>     : Decay File or Keywords: fwp (signal), bkg (non-resonant bkg), dpm (generic bkg)"
  echo -e " <pbeam>   : Momentum of pbar-beam (GeV/c)."
  echo -e " [opt]     : Optional options: if contains 'savesim', 'saveall' or 'ana'\n";
  echo -e "Example 1 : sbatch -a1-20 [options] jobsim_complete.sh fwp 1000 llbar_fwp.DEC"
  echo -e "Example 2 : ./jobsim_complete.sh fwp 100 llbar_fwp.DEC\n"
  exit 1
fi


# Lustre Storage
# LUSTRE=/lustre/$(id -g -n)/$USER
LUSTRE_HOME="/lustre/panda/"$USER


# Working Directory
nyx=$LUSTRE_HOME"/bma"


# Data Storage
tmpdir="/tmp/"$USER
_target=$nyx"/data"


# Init PandaRoot
#. $LUSTRE_HOME"/pandaroot/install-dev/bin/config.sh" -p   # Debian 8 (dev)
. $LUSTRE_HOME"/CENTOS/v12.0.3-install/bin/config.sh" -p  # CentOS 7 (v12.0.3)
#. $LUSTRE_HOME"/CENTOS/dev-install/bin/config.sh" -p       # CentOS 7 (dev)



echo -e "\n";


# Defaults
prefix=llbar                # output file naming
nevt=1000                   # number of events
dec="llbar_fwp.DEC"         # decay file OR keywords [fwp, bkg, dpm]
mom=1.642                   # pbarp with 1.642 GeV/c
mode=0                      # mode for analysis
opt="barrel"                # use opt to do specific tasks
seed=$RANDOM                # random seed for simulation
run=$SLURM_ARRAY_TASK_ID    # Slurm Array ID
TargetMode=0                # Ask for point-like (0) or extended (4) target during simulation.


# User Input
if test "$1" != ""; then
  prefix=$1
fi

if test "$2" != ""; then
  nevt=$2
fi

if test "$3" != ""; then
  dec=$3
fi

if test "$4" != ""; then
  opt=$4
fi


# Deduce Signal/Bkg from .DEC & Create Storage Accordingly.
# e.g. ./jobsim_complete.sh llbar 100 llbar_fwp.DEC 1.642

if [[ $dec == *"fwp"* ]]; then
    IsSignal=true
    _target=$nyx"/fwp"
fi

if [[ $dec == *"bkg"* ]]; then
    IsSignal=false
    _target=$nyx"/bkg"
fi

if [[ $dec == *"dpm"* ]]; then
    IsSignal=false
    _target=$nyx"/dpm"
fi


# Prepend Absolute Path to .DEC File
if [[ $dec == *".dec"* ]]; then
  if [[ $dec != \/* ]] ; then
    dec=$nyx"/"$dec
  fi
fi

if [[ $dec == *".DEC"* ]]; then
  if [[ $dec != \/* ]] ; then
    dec=$nyx"/"$dec
  fi
fi


# Make sure `$_target` Exists
if [ ! -d $_target ]; then
    mkdir -p $_target;
    echo -e "\nThe data dir. at '$_target' created."
else
    echo -e "\nThe data dir. at '$_target' exists."
fi


# IF ARRAY_TASK Used
if test "$run" == ""; then
    tmpdir="/tmp/"$USER
    outprefix=$tmpdir"/"$prefix
    seed=4200
    pidfile=$outprefix"_pid.root"
else
    tmpdir="/tmp/"$USER"_"$SLURM_JOB_ID
    outprefix=$tmpdir"/"$prefix"_"$run
    seed=$SLURM_ARRAY_TASK_ID
    pidfile=$outprefix"_pid.root"
fi


# Make sure $tempdir exists
if [ ! -d $tmpdir ]; then
    mkdir $tmpdir;
    echo -e "The temporary dir. at '$tmpdir' created."
else
    echo -e "The temporary dir. at '$tmpdir' exists."
fi


# ---------------------------------------------------------------
#                              Print Flags
# ---------------------------------------------------------------

echo ""
echo "Lustre Home  : $LUSTRE_HOME"
echo "Working Dir. : $nyx"
echo "Target Dir.  : $_target"
echo "Temp Dir.    : $tmpdir"
echo ""
echo -e "--Macro--"
echo -e "Events    : $nevt"
echo -e "Prefix    : $outprefix"
echo -e "Decay     : $dec"
echo -e "pBeam     : $mom"
echo -e "Seed      : $seed"
echo -e "IsSignal  : $IsSignal"
echo -e "TargetMode: $TargetMode"


# Terminate Script for Testing.
# exit 0;


# ---------------------------------------------------------------
#                            Initiate Simulaton
# ---------------------------------------------------------------

echo ""

# Run Simulation, Digitization
echo "Started Simulating..."
root -l -b -q $nyx"/"sim_complete.C\($nevt,\"$outprefix\",\"$dec\",$mom,$seed,$TargetMode\) > $outprefix"_sim.log" 2>&1

echo "Started Digitization..."
root -l -b -q $nyx"/"digi_complete.C\($nevt,\"$outprefix\"\) > $outprefix"_digi.log" 2>&1

# Run BarrelTrackFinder
if [[ $opt == *"barrel"* ]]; then

    echo "Started Reconstruction (BarrelTrackFinder)..."
    root -l -b -q $nyx"/"reco_complete.C\($nevt,\"$outprefix\"\) > $outprefix"_reco.log" 2>&1
    
    echo "Started PID..."
    root -l -b -q $nyx"/"pid_complete.C\($nevt,\"$outprefix\"\) > $outprefix"_pid.log" 2>&1

fi

# Run HoughTrackFinder
if [[ $opt == *"hough"* ]]; then

    echo "Started Reconstruction (HoughTrackerFinder)..."
    root -l -b -q $nyx"/"houghTrackFinder.C\($nevt,\"$outprefix\"\) > $outprefix"_hough.log" 2>&1

    echo "Started Reconstruction (PzFinder)..."
    root -l -b -q $nyx"/"pzTrackFinder.C\($nevt,\"$outprefix\"\) > $outprefix"_pz.log" 2>&1

    echo "Started Reconstruction (FtsTrackFinder)..."
    root -l -b -q $nyx"/"ftsTrackFinder.C\($nevt,\"$outprefix\"\) > $outprefix"_fts.log" 2>&1
    
    echo "Started PID..."
    root -l -b -q $nyx"/"pid_pzfts.C\($nevt,\"$outprefix\"\) > $outprefix"_pid.log" 2>&1

fi

# Run SttCellTrackFinder
if [[ $opt == *"cell"* ]]; then

    echo "Started Reconstruction (CellTrackerFinder)..."
    root -l -b -q $nyx"/"cellTrackFinder.C\($nevt,\"$outprefix\"\) > $outprefix"_cell.log" 2>&1

    echo "Started Reconstruction (PzFinder)..."
    root -l -b -q $nyx"/"pzTrackFinder.C\($nevt,\"$outprefix\"\) > $outprefix"_pz.log" 2>&1

    echo "Started Reconstruction (FtsTrackFinder)..."
    root -l -b -q $nyx"/"ftsTrackFinder.C\($nevt,\"$outprefix\"\) > $outprefix"_fts.log" 2>&1
    
    echo "Started PID..."
    root -l -b -q $nyx"/"pid_pzfts.C\($nevt,\"$outprefix\"\) > $outprefix"_pid.log" 2>&1

fi

echo "Starting Analysis..."
root -l -b -q $nyx"/"ana_complete.C\($nevt,\"$outprefix\",$IsSignal\) > $outprefix"_ana.log" 2>&1

echo "Finished Simulating..."
echo ""


# ---------------------------------------------------------------
#                            Storing Files
# ---------------------------------------------------------------

NUMEV=`grep 'Generated Events' $outprefix"_sim.log"`
echo $NUMEV >> $outprefix"_pid.log"

# ls in tmpdir to appear in slurmlog
# ls -ltrh $tmpdir

# move root files to target dir
echo "Moving Files from '$tmpdir' to '$_target'"
echo ""

mv $outprefix"_par.root" $_target
mv $outprefix"_sim.root" $_target
mv $outprefix"_sim.log" $_target
mv $outprefix"_digi.root" $_target
mv $outprefix"_digi.log" $_target

if [[ $opt == *"barrel"* ]]; then
    mv $outprefix"_reco.root" $_target
    mv $outprefix"_reco.log" $_target
fi

if [[ $opt == *"cell"* ]]; then
    mv $outprefix"_cell.root" $_target
    mv $outprefix"_cell.log" $_target
    mv $outprefix"_pz.root" $_target
    mv $outprefix"_pz.log" $_target
    mv $outprefix"_fts.root" $_target
    mv $outprefix"_fts.log" $_target
fi

if [[ $opt == *"hough"* ]]; then
    mv $outprefix"_hough.root" $_target
    mv $outprefix"_hough.log" $_target
    mv $outprefix"_pz.root" $_target
    mv $outprefix"_pz.log" $_target
    mv $outprefix"_fts.root" $_target
    mv $outprefix"_fts.log" $_target

fi

mv $outprefix"_pid.root" $_target
mv $outprefix"_pid.log" $_target
mv $outprefix"_ana.root" $_target
mv $outprefix"_ana.log" $_target


#*** Tidy Up ***
rm -rf $tmpdir

echo "The Script has Finished wit SLURM_JOB_ID: $SLURM_JOB_ID."
