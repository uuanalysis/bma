//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug 10 16:11:03 2021 by ROOT version 6.20/08
// from TTree ntpPiMinus/PiMinus Info.
// found on file: bkg1-PiMinus.root
//////////////////////////////////////////////////////////

#ifndef ntpPiMinus_h
#define ntpPiMinus_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpPiMinus {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         piminus_px;
   Float_t         piminus_py;
   Float_t         piminus_pz;
   Float_t         piminus_e;
   Float_t         piminus_p;
   Float_t         piminus_tht;
   Float_t         piminus_phi;
   Float_t         piminus_pt;
   Float_t         piminus_m;
   Float_t         piminus_x;
   Float_t         piminus_y;
   Float_t         piminus_z;
   Float_t         piminus_l;
   Float_t         piminus_chg;
   Float_t         piminus_pdg;

   // List of branches
   TBranch        *b_piminus_px;   //!
   TBranch        *b_piminus_py;   //!
   TBranch        *b_piminus_pz;   //!
   TBranch        *b_piminus_e;   //!
   TBranch        *b_piminus_p;   //!
   TBranch        *b_piminus_tht;   //!
   TBranch        *b_piminus_phi;   //!
   TBranch        *b_piminus_pt;   //!
   TBranch        *b_piminus_m;   //!
   TBranch        *b_piminus_x;   //!
   TBranch        *b_piminus_y;   //!
   TBranch        *b_piminus_z;   //!
   TBranch        *b_piminus_l;   //!
   TBranch        *b_piminus_chg;   //!
   TBranch        *b_piminus_pdg;   //!

   ntpPiMinus(TTree *tree=0);
   virtual ~ntpPiMinus();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TH2F *hpiminus_MomTht;
   TH2F *hpiminus_PzPt;
   TH2F *hpiminus_xy;  //hpiminus_rphi, r = sqrt(x^2 + y^2)
   TH1F *hpiminus_mass;
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
};

#endif

#ifdef ntpPiMinus_cxx
ntpPiMinus::ntpPiMinus(TTree *tree) : fChain(0) 
{

    //*** ---------------------
    //*** Added by ADEEL (START)
    //***
    
    TString path = "";
    TFile* hfile = TFile::Open(path+"ntpPiMinus.root", "RECREATE");
    hpiminus_MomTht = new TH2F("hpiminus_MomTht","|p| vs #theta dist. (piminus);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
    hpiminus_PzPt = new TH2F("hpiminus_PzPt","p_{z} vs p_{t} dist. (piminus);p_{z} [GeV/c];p_{t} [GeV/c]",500,0,1.65,500,0,1.65);
    hpiminus_xy = new TH2F("hpiminus_xy","x vs y dist. (piminus);x [cm];y [cm]",500,-100,100,500,-100,100);
    hpiminus_mass = new TH1F("hpiminus_mass","mass dist. (piminus);m(p) /GeV/c^{2};Entries /GeV/c^{2}",500,0,10);
    
    //*** 
    //*** Added by ADEEL (END)
    //*** ---------------------
    
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("bkg1-PiMinus.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("bkg1-PiMinus.root");
      }
      f->GetObject("ntpPiMinus",tree);

   }
   Init(tree);
   
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   // Call Loop() 
   Loop();

   //Save to ROOT File
   hfile->Write();
   hfile->Close();

   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
}

ntpPiMinus::~ntpPiMinus()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpPiMinus::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpPiMinus::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpPiMinus::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("piminus_px", &piminus_px, &b_piminus_px);
   fChain->SetBranchAddress("piminus_py", &piminus_py, &b_piminus_py);
   fChain->SetBranchAddress("piminus_pz", &piminus_pz, &b_piminus_pz);
   fChain->SetBranchAddress("piminus_e", &piminus_e, &b_piminus_e);
   fChain->SetBranchAddress("piminus_p", &piminus_p, &b_piminus_p);
   fChain->SetBranchAddress("piminus_tht", &piminus_tht, &b_piminus_tht);
   fChain->SetBranchAddress("piminus_phi", &piminus_phi, &b_piminus_phi);
   fChain->SetBranchAddress("piminus_pt", &piminus_pt, &b_piminus_pt);
   fChain->SetBranchAddress("piminus_m", &piminus_m, &b_piminus_m);
   fChain->SetBranchAddress("piminus_x", &piminus_x, &b_piminus_x);
   fChain->SetBranchAddress("piminus_y", &piminus_y, &b_piminus_y);
   fChain->SetBranchAddress("piminus_z", &piminus_z, &b_piminus_z);
   fChain->SetBranchAddress("piminus_l", &piminus_l, &b_piminus_l);
   fChain->SetBranchAddress("piminus_chg", &piminus_chg, &b_piminus_chg);
   fChain->SetBranchAddress("piminus_pdg", &piminus_pdg, &b_piminus_pdg);
   Notify();
}

Bool_t ntpPiMinus::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpPiMinus::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpPiMinus::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpPiMinus_cxx
