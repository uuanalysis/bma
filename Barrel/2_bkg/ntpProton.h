//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug 10 16:09:16 2021 by ROOT version 6.20/08
// from TTree ntpProton/Proton Info.
// found on file: bkg1-Proton.root
//////////////////////////////////////////////////////////

#ifndef ntpProton_h
#define ntpProton_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpProton {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         proton_px;
   Float_t         proton_py;
   Float_t         proton_pz;
   Float_t         proton_e;
   Float_t         proton_p;
   Float_t         proton_tht;
   Float_t         proton_phi;
   Float_t         proton_pt;
   Float_t         proton_m;
   Float_t         proton_x;
   Float_t         proton_y;
   Float_t         proton_z;
   Float_t         proton_l;
   Float_t         proton_chg;
   Float_t         proton_pdg;

   // List of branches
   TBranch        *b_proton_px;   //!
   TBranch        *b_proton_py;   //!
   TBranch        *b_proton_pz;   //!
   TBranch        *b_proton_e;   //!
   TBranch        *b_proton_p;   //!
   TBranch        *b_proton_tht;   //!
   TBranch        *b_proton_phi;   //!
   TBranch        *b_proton_pt;   //!
   TBranch        *b_proton_m;   //!
   TBranch        *b_proton_x;   //!
   TBranch        *b_proton_y;   //!
   TBranch        *b_proton_z;   //!
   TBranch        *b_proton_l;   //!
   TBranch        *b_proton_chg;   //!
   TBranch        *b_proton_pdg;   //!

   ntpProton(TTree *tree=0);
   virtual ~ntpProton();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TH2F *hp_MomTht;
   TH2F *hp_PzPt;
   TH2F *hp_xy;  //hp_rphi, r = sqrt(x^2 + y^2)
   TH1F *hp_mass;
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
};

#endif

#ifdef ntpProton_cxx
ntpProton::ntpProton(TTree *tree) : fChain(0) 
{
    
    //*** ---------------------
    //*** Added by ADEEL (START)
    //***
    
    TString path = "";
    TFile* hfile = TFile::Open(path+"ntpProton.root", "RECREATE");
    hp_MomTht = new TH2F("hp_MomTht","|p| vs #theta dist. (proton);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
    hp_PzPt = new TH2F("hp_PzPt","p_{z} vs p_{t} dist. (proton);p_{z} [GeV/c];p_{t} [GeV/c]",500,0,1.65,500,0,1.65);
    hp_xy = new TH2F("hp_xy","x vs y dist. (proton);x [cm];y [cm]",500,-100,100,500,-100,100);
    hp_mass = new TH1F("hp_mass","mass dist. (proton);m(p) /GeV/c^{2};Entries /GeV/c^{2}",500,0,10);
    
    //*** 
    //*** Added by ADEEL (END)
    //*** ---------------------

// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("bkg1-Proton.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("bkg1-Proton.root");
      }
      f->GetObject("ntpProton",tree);

   }
   Init(tree);
   
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   // Call Loop() 
   Loop();

   //Save to ROOT File
   hfile->Write();
   hfile->Close();

   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
}

ntpProton::~ntpProton()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpProton::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpProton::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpProton::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("proton_px", &proton_px, &b_proton_px);
   fChain->SetBranchAddress("proton_py", &proton_py, &b_proton_py);
   fChain->SetBranchAddress("proton_pz", &proton_pz, &b_proton_pz);
   fChain->SetBranchAddress("proton_e", &proton_e, &b_proton_e);
   fChain->SetBranchAddress("proton_p", &proton_p, &b_proton_p);
   fChain->SetBranchAddress("proton_tht", &proton_tht, &b_proton_tht);
   fChain->SetBranchAddress("proton_phi", &proton_phi, &b_proton_phi);
   fChain->SetBranchAddress("proton_pt", &proton_pt, &b_proton_pt);
   fChain->SetBranchAddress("proton_m", &proton_m, &b_proton_m);
   fChain->SetBranchAddress("proton_x", &proton_x, &b_proton_x);
   fChain->SetBranchAddress("proton_y", &proton_y, &b_proton_y);
   fChain->SetBranchAddress("proton_z", &proton_z, &b_proton_z);
   fChain->SetBranchAddress("proton_l", &proton_l, &b_proton_l);
   fChain->SetBranchAddress("proton_chg", &proton_chg, &b_proton_chg);
   fChain->SetBranchAddress("proton_pdg", &proton_pdg, &b_proton_pdg);
   Notify();
}

Bool_t ntpProton::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpProton::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpProton::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpProton_cxx
