#define ntpLambda_cxx
#include "ntpLambda.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void ntpLambda::Loop()
{
//   In a ROOT session, you can do:
//      root> .L ntpLambda.C
//      root> ntpLambda t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
      
        //*** ---------------------
        //*** Added by ADEEL (START)
        //***


        /* ****************************************************
        *     ...	      pbarp
        *     d0          lambdabar
        *     d0d0        pbar (anti-proton)
        *     d0d1        piplus (pi+)
        *     d1          lambda
        *     d1d0        p (proton)
        *     d1d1        piminus (pi-)
        * *****************************************************/

        // VtxFit Prob, Chi2, ndf
        vtxf_chi2->Fill(VtxFit_chisq);
        vtxf_prob->Fill(VtxFit_prob);
        vtxf_ndf->Fill(VtxFit_ndf);





        //*** 
        //*** Added by ADEEL (END)
        //*** ---------------------
   }
}
