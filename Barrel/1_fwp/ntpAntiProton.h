//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug 10 16:10:18 2021 by ROOT version 6.20/08
// from TTree ntpAntiProton/AntiProton Info.
// found on file: fwp1-AntiProton.root
//////////////////////////////////////////////////////////

#ifndef ntpAntiProton_h
#define ntpAntiProton_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpAntiProton {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         antiproton_px;
   Float_t         antiproton_py;
   Float_t         antiproton_pz;
   Float_t         antiproton_e;
   Float_t         antiproton_p;
   Float_t         antiproton_tht;
   Float_t         antiproton_phi;
   Float_t         antiproton_pt;
   Float_t         antiproton_m;
   Float_t         antiproton_x;
   Float_t         antiproton_y;
   Float_t         antiproton_z;
   Float_t         antiproton_l;
   Float_t         antiproton_chg;
   Float_t         antiproton_pdg;

   // List of branches
   TBranch        *b_antiproton_px;   //!
   TBranch        *b_antiproton_py;   //!
   TBranch        *b_antiproton_pz;   //!
   TBranch        *b_antiproton_e;   //!
   TBranch        *b_antiproton_p;   //!
   TBranch        *b_antiproton_tht;   //!
   TBranch        *b_antiproton_phi;   //!
   TBranch        *b_antiproton_pt;   //!
   TBranch        *b_antiproton_m;   //!
   TBranch        *b_antiproton_x;   //!
   TBranch        *b_antiproton_y;   //!
   TBranch        *b_antiproton_z;   //!
   TBranch        *b_antiproton_l;   //!
   TBranch        *b_antiproton_chg;   //!
   TBranch        *b_antiproton_pdg;   //!

   ntpAntiProton(TTree *tree=0);
   virtual ~ntpAntiProton();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TH2F *hpbar_MomTht;
   TH2F *hpbar_PzPt;
   TH2F *hpbar_xy;
   TH1F *hpbar_mass;
   
   TH2F *hpbar_zR;         // l = R = Perp()
   TH1F *hpbar_r;          // r = sqrt(x^2 + y^2)
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
};

#endif

#ifdef ntpAntiProton_cxx
ntpAntiProton::ntpAntiProton(TTree *tree) : fChain(0) 
{
    
    //*** ---------------------
    //*** Added by ADEEL (START)
    //***
    
    TString path = "";
    TFile* hfile = TFile::Open(path+"ntpAntiProton.root", "RECREATE");
    hpbar_MomTht = new TH2F("hpbar_MomTht","|p| vs #theta dist. (antiproton);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
    hpbar_PzPt = new TH2F("hpbar_PzPt","p_{z} vs p_{t} dist. (antiproton);p_{z} [GeV/c];p_{t} [GeV/c]",500,-1.65,1.65,500,0,1.65);
    hpbar_xy = new TH2F("hpbar_xy","x vs y dist. (antiproton);x [cm];y [cm]",500,-100,100,500,-100,100);
    hpbar_mass = new TH1F("hpbar_mass","mass dist. (antiproton);m(#bar{p}) /GeV/c^{2};Entries /GeV/c^{2}",500,0,1);
    
    hpbar_zR = new TH2F("hpbar_zR","z vs r dist. (antiproton);z [cm];R [cm]",500,-100,100,500,0,50);
    hpbar_r = new TH1F("hpbar_r","r dist. (antiproton);r [cm]",500,0,50);
    
    //*** 
    //*** Added by ADEEL (END)
    //*** ---------------------
    
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("fwp1-AntiProton.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("fwp1-AntiProton.root");
      }
      f->GetObject("ntpAntiProton",tree);

   }
   Init(tree);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   // Call Loop() 
   Loop();

   //Save to ROOT File
   hfile->Write();
   hfile->Close();

   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
   
}

ntpAntiProton::~ntpAntiProton()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpAntiProton::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpAntiProton::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpAntiProton::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("antiproton_px", &antiproton_px, &b_antiproton_px);
   fChain->SetBranchAddress("antiproton_py", &antiproton_py, &b_antiproton_py);
   fChain->SetBranchAddress("antiproton_pz", &antiproton_pz, &b_antiproton_pz);
   fChain->SetBranchAddress("antiproton_e", &antiproton_e, &b_antiproton_e);
   fChain->SetBranchAddress("antiproton_p", &antiproton_p, &b_antiproton_p);
   fChain->SetBranchAddress("antiproton_tht", &antiproton_tht, &b_antiproton_tht);
   fChain->SetBranchAddress("antiproton_phi", &antiproton_phi, &b_antiproton_phi);
   fChain->SetBranchAddress("antiproton_pt", &antiproton_pt, &b_antiproton_pt);
   fChain->SetBranchAddress("antiproton_m", &antiproton_m, &b_antiproton_m);
   fChain->SetBranchAddress("antiproton_x", &antiproton_x, &b_antiproton_x);
   fChain->SetBranchAddress("antiproton_y", &antiproton_y, &b_antiproton_y);
   fChain->SetBranchAddress("antiproton_z", &antiproton_z, &b_antiproton_z);
   fChain->SetBranchAddress("antiproton_l", &antiproton_l, &b_antiproton_l);
   fChain->SetBranchAddress("antiproton_chg", &antiproton_chg, &b_antiproton_chg);
   fChain->SetBranchAddress("antiproton_pdg", &antiproton_pdg, &b_antiproton_pdg);
   Notify();
}

Bool_t ntpAntiProton::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpAntiProton::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpAntiProton::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpAntiProton_cxx
