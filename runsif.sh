#!/bin/sh

# Script to run whole analysis chain using Singularity container.
# The script is based on runideal.sh script with the only exeption 
# of running the PandaRoot from a special Singularity container.

if [ $# -lt 3 ]; then
  echo -e "\nMinimum Three Arguments Are Required\n"
  echo -e "USAGE: sbatch -a<min>-<max> -- runsif.sh <prefix> <nEvents> <dec>\n"
  echo -e " <min>     : Minimum job number"
  echo -e " <max>     : Maximum job number"
  echo -e " <prefix>  : Prefix of output files"
  echo -e " <nevts>   : Number of events to be simulated"
  echo -e " <dec>     : Decay File or Keywords: fwp (signal), bkg (non-resonant bkg), dpm (generic bkg)"
  echo -e " <pbeam>   : Momentum of pbar-beam (GeV/c)."
  echo -e " [opt]     : Optional options: if contains 'savesim', 'saveall' or 'ana'\n";
  echo -e "Example 1 : sbatch -a1-20 [options] runsif.sh fwp 1000 llbar_fwp.DEC"
  echo -e "Example 2 : ./runsif.sh fwp 100 llbar_fwp.DEC\n"
  exit 1
fi

#CONT=$HOME/fair/stable/v12.0.3.sif          # FS (nov20p1), FR (18.6.3), PandaRoot(v12.0.3)
#CONT=$HOME/fair/stable/dev210810.sif        # FS (nov20p1), FR (18.6.3), PandaRoot(dev210810)
#CONT=$HOME/current/containers/v12.0.3.sif   # FS (nov20p1), FR (18.6.3), PandaRoot(dev210810)+Modification
CONT=$HOME/current/containers/pzfinder.sif  # FS (nov20p1), FR (18.6.3), PandaRoot(dev210810)+Modification

# Working Directory
nyx=$HOME"/current/2_deepana/bma"


# Data Storage
tmpdir="/tmp/"$USER
_target=$nyx"/data"

# Default Params
prefix=llbar                # output file for naming.
nevt=100                    # number of events.
dec=llbar_fwp.DEC           # decay file OR keywords [fwp, bkg, dpm].
mom=1.642                   # pbarp with 1.642 GeV/c.
opt=barrel                  # use opt to do specific tasks
IsSignal=true               # signal/background for analysis task.
TargetMode=0                # Ask for point-like (0) or extended (4) target during simulation


# User Input
if test "$1" != ""; then
  prefix=$1
fi

if test "$2" != ""; then
  nevt=$2
fi

if test "$3" != ""; then
  dec=$3
fi

if test "$4" != ""; then
  opt=$4
fi

if [[ $dec == *"fwp"* ]]; then
    IsSignal=true
    _target=$nyx"/fwp"
fi

if [[ $dec == *"bkg"* ]]; then
    IsSignal=false
    _target=$nyx"/bkg"
fi

# Make sure `$_target` Exists
if [ ! -d $_target ]; then
    mkdir -p $_target;
    echo -e "\nThe data dir. at '$_target' created."
else
    echo -e "\nThe data dir. at '$_target' exists."
fi

# Make sure $tempdir exists
if [ ! -d $tmpdir ]; then
    mkdir $tmpdir;
    echo -e "The temporary dir. at '$tmpdir' created."
else
    echo -e "The temporary dir. at '$tmpdir' exists."
fi

#outprefix=$tmpdir"/"$prefix
outprefix=$prefix
# ---------------------------------------------------------------
#                              Print Flags
# ---------------------------------------------------------------

echo ""
echo "Working Home : $nyx"
echo "Target Data  : $_target"
echo "Temp Data.   : $tmpdir"
echo ""
echo -e "--Macro--"
echo -e "Events    : $nevt"
echo -e "Prefix    : $outprefix"
echo -e "Decay     : $dec"
echo -e "pBeam     : $mom"
echo -e "IsSignal  : $IsSignal"
echo -e "TargetMode: $TargetMode"
echo -e "Tracker   : $opt"


# Terminate Script for Testing.
# exit 0;


# ---------------------------------------------------------------
#                            Initiate Simulaton
# ---------------------------------------------------------------

echo "Started Simulation..."
singularity exec $CONT root -l -b -q sim_complete.C\($nevt,\"$outprefix\",\"$dec\"\) > sim.log 2>&1

echo "Started Digitization..."
singularity exec $CONT root -l -b -q digi_complete.C\($nevt,\"$outprefix\"\) > digi.log 2>&1


# Run BarrelTrackFinder
if [[ $opt == *"barrel"* ]]; then

    echo "Started Reconstruction (BarrelTrackFinder)..."
    singularity exec $CONT root -l -b -q reco_complete.C\($nevt,\"$outprefix\"\) > reco.log 2>&1
    
    echo "Started PID..."
    singularity exec $CONT root -l -b -q pid_complete.C\($nevt,\"$outprefix\"\) > pid.log 2>&1

fi

# Run HoughTrackFinder
if [[ $opt == *"hough"* ]]; then

    echo "Started Reconstruction (HoughTrackerFinder)..."
    singularity exec $CONT root -l -b -q houghTrackFinder.C\($nevt,\"$outprefix\"\) > hough.log 2>&1

    echo "Started Reconstruction (PzFinder)..."
    singularity exec $CONT root -l -b -q pzTrackFinder.C\($nevt,\"$outprefix\",\"$opt\"\) > pz.log 2>&1

    echo "Started Reconstruction (FtsTrackFinder)..."
    singularity exec $CONT root -l -b -q ftsTrackFinder.C\($nevt,\"$outprefix\"\) > fts.log 2>&1
    
    echo "Started PID..."
    singularity exec $CONT root -l -b -q pid_pzfts.C\($nevt,\"$outprefix\"\) > pid.log 2>&1

fi

# Run SttCellTrackFinder
if [[ $opt == *"cell"* ]]; then

    echo "Started Reconstruction (CellTrackerFinder)..."
    singularity exec $CONT root -l -b -q cellTrackFinder.C\($nevt,\"$outprefix\"\) > cell.log 2>&1

    echo "Started Reconstruction (PzFinder)..."
    singularity exec $CONT root -l -b -q pzTrackFinder.C\($nevt,\"$outprefix\",\"$opt\"\) > pz.log 2>&1

    echo "Started Reconstruction (FtsTrackFinder)..."
    singularity exec $CONT root -l -b -q ftsTrackFinder.C\($nevt,\"$outprefix\"\) > fts.log 2>&1
    
    echo "Started PID..."
    singularity exec $CONT root -l -b -q pid_pzfts.C\($nevt,\"$outprefix\"\) > pid.log 2>&1

fi


echo "Starting Analysis Task (User-defined)..."
singularity exec $CONT root -l -q -b ana_complete.C\($nevt,\"$outprefix\",$IsSignal\) > ana.log 2>&1

echo "Script has finished..."
echo ""


# ---------------------------------------------------------------
#                            Storing Files
# ---------------------------------------------------------------
echo "Moving Files from '$tmpdir' to '$_target'"

# move root files to target dir
cp $outprefix"_par.root" $_target
cp $outprefix"_sim.root" $_target
cp $outprefix"_sim.log" $_target
cp $outprefix"_digi.root" $_target
cp $outprefix"_digi.log" $_target

if [[ $opt == *"barrel"* ]]; then
    cp $outprefix"_reco.root" $_target
    cp $outprefix"_reco.log" $_target
fi

if [[ $opt == *"cell"* ]]; then
    cp $outprefix"_cell.root" $_target
    cp $outprefix"_cell.log" $_target
    cp $outprefix"_pz.root" $_target
    cp $outprefix"_pz.log" $_target
    cp $outprefix"_fts.root" $_target
    cp $outprefix"_fts.log" $_target
fi

if [[ $opt == *"hough"* ]]; then
    cp $outprefix"_hough.root" $_target
    cp $outprefix"_hough.log" $_target
    cp $outprefix"_pz.root" $_target
    cp $outprefix"_pz.log" $_target
    cp $outprefix"_fts.root" $_target
    cp $outprefix"_fts.log" $_target

fi

cp $outprefix"_pid.root" $_target
cp $outprefix"_pid.log" $_target
cp $outprefix"_ana.root" $_target
cp $outprefix"_ana.log" $_target

#*** Tidy Up ***
rm -rf $tmpdir

echo "The Script has Finished."
